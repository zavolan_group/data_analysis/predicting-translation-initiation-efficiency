import numpy as np
import pandas as pd


'''
method extracts gene_id and transcript_id
from attribute columns in gtf file
'''
def extract_gene_id_transcript_id(att_col):
    line = att_col.split()

    gi = line.index('gene_id')
    ti = line.index('transcript_id')

    if gi != -1 and ti!= -1 and gi+1 < len(line) and ti+1<len(line):
        return line[gi+1].strip('";').strip('"'), line[ti+1].strip('";').strip('"')
    else:
        return "gene_ID NA", "transcript_ID NA"


'''
method returns data frame
with mutations
on given exon
'''
def get_mutations_on_exon(chrom,start,end,var_df):
    ret_df = var_df[(var_df['chrom'] == chrom) & (var_df['pos'] >= start) & (var_df['pos'] <= end)]
    return ret_df


var_utrs_gtf = pd.read_csv('variant_utr_list.gtf',sep='\t',low_memory=False)
var_vcf = pd.read_csv('clinvar_fiveprimeutrs.vcf',sep='\t',low_memory=False)

var_utrs_gtf.columns = ['chrom','source','feature','start','end','score','strand','frame','attribute']
var_vcf.columns = ['chrom','pos','id','ref','alt','qual','filter','info']

var_utrs_gtf = var_utrs_gtf.sort_values(by=['chrom','start'],ignore_index=True)
var_vcf = var_vcf.sort_values(by=['chrom','pos'],ignore_index=True)

var_utrs_gtf['gene_ID'], var_utrs_gtf['transcript_ID'] = zip(*var_utrs_gtf['attribute'].apply(lambda x : extract_gene_id_transcript_id(x)))

print("chrom\trel_pos\ttranscript_ID\tstrand\tpos\tid\tref_nt\talt_nt\tinfo")
for group_name, df_group in var_utrs_gtf.groupby('transcript_ID'):
    sorted_group = df_group.sort_values(by=['start']) #just to be on the safe side

    #get mutations on given exon
    offset = 0
    for _, row in sorted_group.iterrows():
        mut_on_ex = get_mutations_on_exon(row['chrom'],row['start'],row['end'],var_vcf)

        for _, mut in mut_on_ex.iterrows():
            print(str(mut['chrom'])+"\t"+str(offset+mut['pos']-row['start'])+"\t"+str(row['transcript_ID'])+"\t"+str(row['strand'])+"\t"+str(mut['pos'])+"\t"+str(mut['id'])+"\t"+mut['ref']+"\t"+mut['alt']+"\t"+mut['info'])

        offset += row['end'] - row['start'] + 1

