#!/bin/bash

clinvar_url="https://ftp.ncbi.nlm.nih.gov/pub/clinvar/vcf_GRCh38/clinvar.vcf.gz"
gtf_filepath="~/remote_scicore/riboseq_pipeline/snakemake/annotation/homo_sapiens/Homo_sapiens.GRCh38.105.chr.gtf"


wget ${clinvar_url}
gunzip clinvar.vcf.gz

bedtools intersect -wa -a ${gtf_filepath} -b clinvar.vcf | awk '$3 ~ /five_prime_utr/' | sort -u > variant_utr_list.gtf

grep '5_prime_UTR_variant' clinvar.vcf > clinvar_fiveprimeutrs.vcf

echo "Calculate transcript coordinates of point mutations."
python3 vcf2utrcoordinates.py > clinvar_rel_coordinates.tsv

echo "Construct mutated UTRs."
python3 construct_mutated_utrs.py

echo "Add CDS sequences"
python3 get_CDS_sequences.py

echo "Calculate UTR metadata."
python3 add_metadata.py


