import numpy as np
import pandas as pd
from pybiomart import Dataset


'''
method converts sequence
into mutated version
'''
def get_mutated_sequence(seq,position,original,mutation):
    mut_len = len(original)

    if (seq[position:(position+mut_len)] == original):
        ret = seq[:position] + mutation + seq[(position+mut_len):]
    else:
        ret = "Sequence mismatch"

    return ret


'''
method extracts feature from info line
'''
def extract_feature(info_line,feature):
    if (feature+'=' in info_line):
        line = info_line.split(";")

        li = -1
        for i,s in enumerate(line):
            if feature+'=' in s:
                li = i

        if (i == -1):
            return "No "+feature
        else:
            return line[li].replace(feature+"=","")
    else:
        return "No "+feature


'''
BEGIN script
'''
var_rel_coords = pd.read_csv('clinvar_rel_coordinates.tsv',sep="\t",low_memory=False)


dataset = Dataset(name='hsapiens_gene_ensembl',host='http://www.ensembl.org')

#load 5' UTRs from ensembl
raw_utrs1 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['1','2','3','4']})
print('Chromosomes 1-4 queried')
raw_utrs2 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['5','6','7','8']})
print('Chromosomes 5-8 queried')
raw_utrs3 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['9','10','11','12']})
print('Chromosomes 9-12 queried')
raw_utrs4 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['13','14','15','16']})
print('Chromosomes 13-16 queried')
raw_utrs5 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['17','18','19','20']})
print('Chromosomes 17-20 queried')
raw_utrs6 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['21','22','MT','X','Y','GL000009.2','GL000194.1','GL000195.1','GL000205.2','GL000213.1','GL000216.2','GL000218.1','GL000219.1','GL000220.1','GL000225.1','KI270442.1','KI270711.1','KI270713.1','KI270721.1','KI270726.1','KI270727.1','KI270728.1','KI270731.1','KI270733.1','KI270734.1','KI270744.1','KI270750.1']})
print('Chromosomes 21, 22, MT, X, Y, unlocalized queried')
raw_utrs = pd.concat([raw_utrs1,raw_utrs2,raw_utrs3,raw_utrs4,raw_utrs5,raw_utrs6],axis=0)

utrs = raw_utrs[raw_utrs["5' UTR"] != "Sequence unavailable"]
utrs.columns = ['UTR','gene_ID','transcript_ID']

var_df = pd.merge(var_rel_coords,utrs,how='inner',on='transcript_ID')

var_df['clinical_significance'] = var_df['info'].apply(lambda x : extract_feature(x,'CLNSIG'))
var_df['rev_stat'] = var_df['info'].apply(lambda x : extract_feature(x,'CLNREVSTAT'))
var_df['mutated_utr'] = var_df.apply(lambda x : get_mutated_sequence(x.UTR,x.rel_pos,x.ref_nt,x.alt_nt), axis=1)


var_df = var_df[(var_df['mutated_utr'] != "Sequence mismatch")]

var_df = var_df[['gene_ID','transcript_ID','UTR','mutated_utr','rel_pos','ref_nt','alt_nt','clinical_significance','rev_stat','info']]

var_df.to_csv("mutated_sequences.tsv",sep="\t",index=False)


