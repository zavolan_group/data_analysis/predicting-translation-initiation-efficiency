import pandas as pd
import numpy as np
import RNA
from Bio.SeqUtils import gc_fraction


'''
auxiliary method
returns number of upstream AUGs
'''
def get_number_uAUGs(seq):
    inframe = 0
    outframe = 0

    s = seq.upper()

    for i in range(len(s)-2):
        if s[i:i+3] == 'ATG':
            if (len(s) - i) % 3 == 0:
                inframe += 1
            else:
                outframe += 1

    return inframe, outframe


'''
begin script
'''
df = pd.read_csv("mutated_sequences.tsv", sep='\t', low_memory=False)

#metadata
print("Calculate UTR metadata:")

df["UTR_length"] = df["mutated_utr"].str.len()
print("Calculated UTR length.")

#df['5p_cap_folding_energy'] = df["mutated_utr"].apply(lambda x : RNA.fold(x[:70])[1])
#print("Calculated cap folding energy.")
df['normalized_5p_folding_energy'] = df["mutated_utr"].apply(lambda x : RNA.fold(x)[1]) / df['UTR_length']
print("Calculated folding energy.")

df['GC_content'] = df["mutated_utr"].apply(lambda x : gc_fraction(x))
print("Calculated GC content.")

df['number_inframe_uAUGs'], df['number_outframe_uAUGs'] = zip(*df["mutated_utr"].apply(lambda x : get_number_uAUGs(x)))
df['number_uAUGs'] = df['number_inframe_uAUGs'] + df['number_outframe_uAUGs']
print("Number of uAUGs calculated.")

df['number_CGs'] = df['mutated_utr'].apply(lambda x : x.upper().count('CG'))
print("Number of uCGs calculated.")

#add CDS
CDS_df = pd.read_csv("CDS_sequences.tsv",sep="\t",low_memory=False)
df = pd.merge(df,CDS_df,on='transcript_ID')
print("Added CDS.")

#add exons
exon_df = pd.read_csv("exons_per_transcript_human.tsv",sep='\t')
df = pd.merge(df,exon_df,left_on="transcript_ID",right_on="transcript_id")
print("Added number of exons.")

df = df[['gene_ID','transcript_ID','mutated_utr','rel_pos','ref_nt','alt_nt','clinical_significance','rev_stat','info','CDS','ORF_length','log_ORF_length','number_exons','UTR_length','normalized_5p_folding_energy','GC_content','number_inframe_uAUGs','number_outframe_uAUGs','number_uAUGs','number_CGs']]

df = df[(df['mutated_utr'].str.find("N") == -1) & (df['CDS'].str.find("N") == -1) & (df['alt_nt'] != '.')]


df.to_csv('mutated_sequences_with_metadata.tsv', sep='\t', index=False)
