#!/usr/bin/env python
import pysam
import json
import numpy as np
from numpy import inf
import pandas as pd
from Bio import SeqIO

riboseq_alignment_filename = 'riboseq_data/transcripts.mapped.unique.sorted.bam'
riboseq_offset_filename = 'riboseq_data/alignment_offset.json'
riboseq_coordinate_filename = 'riboseq_data/transcript_id_gene_id_CDS.tsv'

fasta_filename = 'riboseq_data/longest_pc_transcript_per_gene.fa'

RNAseq_filename = 'rna_seq_data/all_rna_on_cds_reads.tsv'
column = 'CDS_RNAseq_union_HEK293_alexaki_WTs'

output_filename = 'TE.tsv'


'''
read offset file
'''
def read_offset(filename):
	with open(filename, 'r') as json_file:
		data = json_file.read()

	return json.loads(data)


'''
read coordinate file
'''
def read_coordinate_file(filename):
	CDS_start_ind = {}
	CDS_stop_ind = {}
	CDS_length = {}
	transcript_ID_index = {}
	index_transcript_ID = {}
	gene_ID_index = {}
	index_gene_ID = {}
	ctr = 0
	with open(filename) as CDS_coordinates:
		for line in CDS_coordinates:
			sp_line = line.strip().split("\t")
			transcript_id = sp_line[0]
			gene_id = sp_line[1]
			CDS_start_ind[transcript_id] = int(sp_line[2]) - 1
			CDS_stop_ind[transcript_id] = int(sp_line[3])
			CDS_length[transcript_id] = CDS_stop_ind[transcript_id] - CDS_start_ind[transcript_id]
			transcript_ID_index[transcript_id] = ctr
			index_transcript_ID[ctr] = transcript_id
			gene_ID_index[gene_id] = ctr
			index_gene_ID[ctr] = gene_id
			ctr += 1

	return CDS_start_ind, CDS_stop_ind, CDS_length, transcript_ID_index, index_transcript_ID, gene_ID_index, index_gene_ID


'''
read bam file to counts
'''
def read_bam_to_counts(filename,CDS_length,CDS_start_ind,CDS_stop_ind,offsets):
	bam = pysam.AlignmentFile(filename, "rb")
	max_len_CDS = max(CDS_length.values()) // 3
	nbr_genes = len(CDS_length)

	riboseq_counts = np.zeros(shape=(nbr_genes),dtype='int32')

	for read in bam.fetch():
		read_start = read.reference_start
		transcript_name = read.reference_name
		CDS_start = CDS_start_ind[transcript_name]
		CDS_stop = CDS_stop_ind[transcript_name]

		if read.is_reverse:
			continue

		if str(len(str(read.seq))) in offsets:
			codon_position = read_start - CDS_start + offsets[str(len(read.seq))]

			#only CDS reads
			if codon_position in range(0,CDS_stop-CDS_start):
				riboseq_counts[transcript_ID_index[transcript_name]] += 1
	bam.close()

	return riboseq_counts


'''
load file with RNAseq TPM levels
into dictionary
'''
def load_RNAseq_counts(filename,column):
	df_TPM = pd.read_csv(filename,sep='\t',usecols=['gene_id',column])
	return dict(zip(df_TPM['gene_id'],df_TPM[column]))


'''
compute TE
'''
def compute_translation_efficiency(CDS_length,index_gene_ID,riboseq_counts,RNAseq_counts):
	nbr_genes = len(CDS_length)

	TE = np.zeros(shape=(nbr_genes),dtype='float32')
	counts = np.zeros(shape=(nbr_genes),dtype='float32')

	for i,gene_ID in index_gene_ID.items():
		#convert TPM to counts
		counts[i] = float(RNAseq_counts[gene_ID])

	counts = np.reciprocal(counts)
	counts[counts == inf] = 0.0

	norm_riboseq = sum(riboseq_counts)
	norm_RNAseq = sum(RNAseq_counts.values())

	TE = riboseq_counts * counts * norm_RNAseq / norm_riboseq

	return TE


'''
BEGIN SCRIPT
'''
offsets = read_offset(riboseq_offset_filename)

CDS_start_ind, CDS_stop_ind, CDS_length, transcript_ID_index, index_transcript_ID, gene_ID_index, index_gene_ID = read_coordinate_file(riboseq_coordinate_filename)

riboseq_counts = read_bam_to_counts(riboseq_alignment_filename,CDS_length,CDS_start_ind,CDS_stop_ind,offsets)

RNAseq_counts = load_RNAseq_counts(RNAseq_filename,column)


TE = compute_translation_efficiency(CDS_length,index_gene_ID,riboseq_counts,RNAseq_counts)

w = open(output_filename,'w')
w.write('transcript_ID\tgene_ID\ttotal_reads_riboseq\tTE\n')
for i,ie in enumerate(TE):
	w.write(index_transcript_ID[i]+'\t'+index_gene_ID[i]+'\t'+str(riboseq_counts[i])+'\t'+str(TE[i])+'\n')
w.close()


