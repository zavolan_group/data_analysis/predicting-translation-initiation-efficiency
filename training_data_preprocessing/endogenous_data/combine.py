'''
This program calculates the initiation efficiency 
based on RNA seq and ribo seq data

averages overmultiple replicates
'''
import sys
import pandas as pd
import numpy as np
import RNA
from Bio.SeqUtils import gc_fraction


TE_filename = 'TE.tsv'
UTRs_filename = '5putrs_RNAseq.tsv'
exons_filename = 'exons_per_transcript_human.tsv'
CDS_filename = 'CDS_sequences.tsv'
TIN_filename = 'rna_seq_data/TIN_scores_replicates.tsv'


'''
auxiliary method
returns number of upstream AUGs
'''
def get_number_uAUGs(seq):
    inframe = 0
    outframe = 0

    s = seq.upper()

    for i in range(len(s)-2):
        if s[i:i+3] == 'ATG':
            if (len(s) - i) % 3 == 0:
                inframe += 1
            else:
                outframe += 1

    return inframe, outframe


#create averaged riboseq dataset
df_riboseq = pd.read_csv(TE_filename, sep='\t',usecols=['gene_ID','transcript_ID','total_reads_riboseq','TE'])

#load RNA seq dataset
df_rnaseq = pd.read_csv(UTRs_filename, sep='\t',usecols=['Gene stable ID','Name',"5' UTR"])

df = pd.merge(df_riboseq, df_rnaseq, left_on = 'gene_ID', right_on='Gene stable ID')


#load number of exons
df_exon_number = pd.read_csv(exons_filename,sep='\t',usecols=['transcript_id','number_exons'])

df = pd.merge(df,df_exon_number,left_on='transcript_ID',right_on='transcript_id')

#load first N nucleotides of CDS
df_CDS = pd.read_csv(CDS_filename,sep='\t',usecols=['transcript_ID','CDS','ORF_length','log_ORF_length'])

df = pd.merge(df,df_CDS,on='transcript_ID')


#load TIN scores
df_TIN = pd.read_csv(TIN_filename,sep='\t')
df_TIN.columns = ['transcript_ID','TIN_rep1','TIN_rep2','TIN_rep3']
df_TIN['sq_avg_TIN'] = np.sqrt((np.square(df_TIN['TIN_rep1']) + np.square(df_TIN['TIN_rep2']) + np.square(df_TIN['TIN_rep3'])) / 3.0)

df = pd.merge(df,df_TIN,on='transcript_ID',how='left')
df = df.fillna(0.0)


#metadata
print("Calculate UTR metadata:")

df["UTR_length"] = df["5' UTR"].str.len()
print("Calculated UTR length.")

df['5p_cap_folding_energy'] = df["5' UTR"].apply(lambda x : RNA.fold(x[:70])[1])
print("Calculated cap folding energy.")
df['normalized_5p_folding_energy'] = df["5' UTR"].apply(lambda x : RNA.fold(x)[1]) / df['UTR_length']
print("Calculated folding energy.")

df['GC_content'] = df["5' UTR"].apply(lambda x : gc_fraction(x))
print("Calculated GC content.")

df['number_inframe_uAUGs'], df['number_outframe_uAUGs'] = zip(*df["5' UTR"].apply(lambda x : get_number_uAUGs(x)))
df['number_uAUGs'] = df['number_inframe_uAUGs'] + df['number_outframe_uAUGs']
print("Number of uAUGs calculated.")



df = df[['gene_ID','Name',"5' UTR",'CDS','total_reads_riboseq','TIN_rep1','TIN_rep2','TIN_rep3','sq_avg_TIN', 'UTR_length','ORF_length','number_exons','5p_cap_folding_energy','log_ORF_length','normalized_5p_folding_energy','GC_content','number_inframe_uAUGs','number_outframe_uAUGs','number_uAUGs','TE']]

#drop the transcript ID from riboseq (=longest transcript), use transcript ID of dominant transcript in RNAseq
df = df.rename(columns={'Name' : 'transcript_ID'})

df = df[(df['TE'] > 0.0)]

df.to_csv('init_effs.tsv', sep='\t', index=False)

