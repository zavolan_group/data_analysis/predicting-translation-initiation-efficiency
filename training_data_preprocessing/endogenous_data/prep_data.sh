#!/bin/bash

#do RNA seq analysis
python3 rna_seq_get_5UTR_freq.py
echo "RNAseq + transcript frequencies ready."

#calculate (ramp-corrected) initiation efficiency
python3 calc_TE.py
echo "Ramp-corrected initiation efficiency calculated."

#extract first couple nts of the ORF
python3 get_CDS_sequences.py
echo "CDS sequences extracted."

#marry all of them
python3 combine.py
