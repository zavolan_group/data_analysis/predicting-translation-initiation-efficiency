'''
This program determines the most likely transcript per gene 
from RNA seq input (kallisto or salmon)

INPUT:
	- Kallisto or Salmon summary file
	- list for transcript and gene IDs
	- wifi connection for downloading ensembl data
'''
import numpy as np
import pandas as pd
from pybiomart import Dataset

kallisto_filename = "rna_seq_data/transcripts_numreads.tsv"
sample_name = "HEK293_alexaki_WT_union"

dataset = Dataset(name='hsapiens_gene_ensembl',host='http://www.ensembl.org')

#load 5' UTRs from ensembl
raw_utrs1 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['1','2','3','4']})
print('Chromosomes 1-4 queried')
raw_utrs2 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['5','6','7','8']})
print('Chromosomes 5-8 queried')
raw_utrs3 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['9','10','11','12']})
print('Chromosomes 9-12 queried')
raw_utrs4 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['13','14','15','16']})
print('Chromosomes 13-16 queried')
raw_utrs5 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['17','18','19','20']})
print('Chromosomes 17-20 queried')
raw_utrs6 = dataset.query(attributes=['ensembl_gene_id', 'ensembl_transcript_id','5utr'],filters={'chromosome_name': ['21','22','MT','X','Y','GL000009.2','GL000194.1','GL000195.1','GL000205.2','GL000213.1','GL000216.2','GL000218.1','GL000219.1','GL000220.1','GL000225.1','KI270442.1','KI270711.1','KI270713.1','KI270721.1','KI270726.1','KI270727.1','KI270728.1','KI270731.1','KI270733.1','KI270734.1','KI270744.1','KI270750.1']})
print('Chromosomes 21, 22, MT, X, Y, unlocalized queried')
raw_utrs = pd.concat([raw_utrs1,raw_utrs2,raw_utrs3,raw_utrs4,raw_utrs5,raw_utrs6],axis=0)
utrs = raw_utrs[raw_utrs["5' UTR"] != "Sequence unavailable"]


'''
load kallisto/salmon RNA seq data
INPUT:
	- tx2geneID.tsv from kallisto summary in zarp
	- transcript_counts.tsv (need to insert transcript_ID header by hand!)
join the two above data sets based on the transcript ID
'''
df_counts_isoforms = pd.read_csv(kallisto_filename, sep='\t',usecols=(['Name',sample_name]))

#merge both
df = pd.merge(df_counts_isoforms, utrs, left_on ='Name' , right_on = 'Transcript stable ID')
df.drop(columns=['Transcript stable ID'],inplace=True)
df = df.rename(columns={sample_name : 'counts'})
df = df[df['counts'] > 0]

#only keep the transcripts with the max number of counts per gene
idx = df.groupby('Gene stable ID')['counts'].transform(max) == df['counts']

df = df[idx]
df = df[["Name","Gene stable ID","5' UTR","counts"]]

df.to_csv('5putrs_RNAseq.tsv', sep='\t', index=False)
