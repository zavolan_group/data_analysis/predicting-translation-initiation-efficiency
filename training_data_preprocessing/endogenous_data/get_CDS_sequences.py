#!/usr/bin/env python
import numpy as np
import pandas as pd
from Bio import SeqIO


transcriptome_filename = 'riboseq_data/human_transcriptome.fa'

#load transcriptome fasta file as dict
seq_dict = {}
CDS_start_dict = {}
CDS_stop_dict = {}
CDS_length_dict = {}
for read in SeqIO.parse(transcriptome_filename,'fasta'):
	if 'CDS=' in read.description:
		seq_dict[read.id] = str(read.seq)
		s = read.description.strip().split("CDS=")[-1].split('-')
		CDS_start_dict[read.id] = int(s[0])
		CDS_stop_dict[read.id] = int(s[1])
		CDS_length_dict[read.id] = int(s[1]) - int(s[0]) + 1

CDS_seqs = {}
for t,s in seq_dict.items():
	CDS_seqs[t] = s[(CDS_start_dict[t] - 1):CDS_stop_dict[t]]

df1 = pd.DataFrame(CDS_length_dict.items(),columns=['transcript_ID','ORF_length'])
df2 = pd.DataFrame(CDS_seqs.items(),columns=['transcript_ID','CDS'])

df = pd.merge(df1,df2,on='transcript_ID')

df['log_ORF_length'] = np.log(df['ORF_length'])

df = df[['transcript_ID','CDS','ORF_length','log_ORF_length']]

df.to_csv('CDS_sequences.tsv', sep='\t', index=False)
