import pandas as pd
import numpy as np
import RNA
from Bio.SeqUtils import gc_fraction


'''
auxiliary method
returns number of upstream AUGs
'''
def get_number_uAUGs(seq):
    inframe = 0
    outframe = 0

    s = seq.upper()

    for i in range(len(s)-2):
        if s[i:i+3] == 'ATG':
            if (len(s) - i) % 3 == 0:
                inframe += 1
            else:
                outframe += 1

    return inframe, outframe


'''
begin script
'''
df = pd.read_csv("GSM4084997_varying_length_25to100_top_105k.csv", low_memory=False, usecols=['utr','total_reads','rl'])

df = df[(df['rl'] > 0.0)]

#metadata
print("Calculate UTR metadata:")

df["UTR_length"] = df["utr"].str.len()
print("Calculated UTR length.")

df['5p_cap_folding_energy'] = df["utr"].apply(lambda x : RNA.fold(x[:70])[1])
print("Calculated cap folding energy.")
df['normalized_5p_folding_energy'] = df["utr"].apply(lambda x : RNA.fold(x)[1]) / df['UTR_length']
print("Calculated folding energy.")

df['GC_content'] = df["utr"].apply(lambda x : gc_fraction(x))
print("Calculated GC content.")

df['number_inframe_uAUGs'], df['number_outframe_uAUGs'] = zip(*df["utr"].apply(lambda x : get_number_uAUGs(x)))
df['number_uAUGs'] = df['number_inframe_uAUGs'] + df['number_outframe_uAUGs']
print("Number of uAUGs calculated.")

df['number_CGs'] = df['utr'].apply(lambda x : x.upper().count('CG'))
print("Number of uCGs calculated.")


df = df[['utr','UTR_length','5p_cap_folding_energy','normalized_5p_folding_energy','GC_content','number_inframe_uAUGs','number_outframe_uAUGs','number_uAUGs','number_CGs','total_reads','rl']]


df.to_csv('opt100_DS_with_metadata.tsv',sep='\t', index=False)
