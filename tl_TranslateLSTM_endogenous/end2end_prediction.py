import sys
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import matplotlib.pyplot as plt
from scipy import stats
from sklearn import preprocessing
import random
import RNA
from Bio.SeqUtils import gc_fraction
import joblib
import os



seed=1337
random.seed(seed)
np.random.seed(seed)
tf.random.set_seed(seed)


prim_input_col="5' UTR" #column name for the primary input (5' UTR)
sec_input_col='CDS' #column name for the secondary input (CDS)
metadata_cols = ['UTR_length','log_ORF_length','number_exons','normalized_5p_folding_energy','GC_content','number_outframe_uAUGs','number_inframe_uAUGs']
output_col='TE'

model_path = 'tl_TranslateLLM_HEK293.h5'
prediction_data_path = '<placeholder for tsv file name>'    #needs to contain non-utr sequence based features, i.e. number exons and log_ORF_length, as well as the primary and secondary input columns (5' UTR and CDS)


#nucleotide dictionary
nt_dict = {'A' : 0, 'C' : 1, 'G' : 2, 'T' : 3, 'a' : 0, 'c' : 1, 'g' : 2, 't' : 3}



'''
auxiliary method converts input sequences to float vectors
'''
def convert_seq2vec(in_string):
	out_vec = []
	for i in range(0,len(in_string)):
		out_vec.append(nt_dict[in_string[i]])
	return keras.utils.to_categorical(out_vec,num_classes=4,dtype='float32')



'''
method reads test data file
keeps the 10 best and 10 worst performing sequences
'''
def read_sequences(data_file_path):
	#load raw data from csv
	if '.tsv' in data_file_path:
		raw_data = pd.read_csv(data_file_path, low_memory=False, sep="\t")
	else:
		raw_data = pd.read_csv(data_file_path, low_memory=False)

	return raw_data



'''
auxiliary method
returns number of upstream AUGs
'''
def get_number_uAUGs(seq):
    inframe = 0
    outframe = 0

    s = seq.upper()

    for i in range(len(s)-2):
        if s[i:i+3] == 'ATG':
            if (len(s) - i) % 3 == 0:
                inframe += 1
            else:
                outframe += 1

    return inframe, outframe



'''
method calculates
utr sequence-based
metadata for the UTRs to predict
'''
def compute_utr_sequence_based_metadata(data_frame):
	utrs = data_frame[prim_input_col].to_numpy()

	data_frame['UTR_length'] = data_frame[prim_input_col].str.len()

	if 'normalized_5p_folding_energy' in metadata_cols:
		data_frame['normalized_5p_folding_energy'] = data_frame[prim_input_col].apply(lambda x : RNA.fold(x)[1]) / data_frame['UTR_length']

	if '5p_cap_folding_energy' in metadata_cols:
		data_frame['5p_cap_folding_energy'] = data_frame[prim_input_col].apply(lambda x : RNA.fold(x[:70])[1])

	if 'GC_content' in metadata_cols:
		data_frame['GC_content'] = data_frame[prim_input_col].apply(lambda x : gc_fraction(x))

	if ('number_inframe_uAUGs' in metadata_cols) or ('number_outframe_uAUGs' in metadata_cols):
		data_frame['number_inframe_uAUGs'], data_frame['number_outframe_uAUGs'] = zip(*data_frame[prim_input_col].apply(lambda x : get_number_uAUGs(x)))
		data_frame['number_uAUGs'] = data_frame['number_inframe_uAUGs'] + data_frame['number_outframe_uAUGs']

	return data_frame



'''
method makes predictions with model for input data frame
'''
def predict_IE(data_frame,model_path):
	model = keras.models.load_model(model_path)

	#need one-hot encoding for sequence data
	metadata_input = pd.DataFrame()
	for md in metadata_cols:
		tsc = joblib.load('scalers/' + md + '_scaler.gz')
		metadata_input[md] = pd.DataFrame(tsc.transform(data_frame[md].values.reshape(-1,1)))

	output_scaler = joblib.load('scalers/output_scaler.gz')

	tmp = data_frame[prim_input_col].to_numpy()
	tmp_test = []
	for t in tmp:
		tmp_test.append(convert_seq2vec(t))
	prim_seq_input = keras.utils.pad_sequences(tmp_test,maxlen=max(data_frame['UTR_length'].to_numpy()),dtype='float32',padding='pre',value=-1.0)

	data_frame[sec_input_col] = data_frame[sec_input_col].astype(str).str[:100]

	tmp = data_frame[sec_input_col].to_numpy()
	tmp_test = []
	for t in tmp:
		tmp_test.append(convert_seq2vec(t))
	sec_seq_input = keras.utils.pad_sequences(tmp_test,maxlen=100,dtype='float32',padding='pre',value=-1.0)

	data_frame['predicted_output'] = output_scaler.inverse_transform(model.predict([[prim_seq_input,np.zeros(shape=(len(prim_seq_input),5),dtype='float32')],sec_seq_input,metadata_input]))[:,0]

	return data_frame



'''
begin script
'''
prediction_df = read_sequences(prediction_data_path)
print("\nSequences read")

prediction_df = compute_utr_sequence_based_metadata(prediction_df)
print("\n5' UTR features calculated")

prediction_df = predict_IE(prediction_df,model_path)
print("Prediction successful")

prediction_df.to_csv("predictions_"+output_col+".tsv",sep="\t",index=False)
