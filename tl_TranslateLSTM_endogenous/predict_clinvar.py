import sys
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import matplotlib.pyplot as plt
from scipy import stats
from sklearn import preprocessing
import random
import joblib
import os


seed=1337
random.seed(seed)
np.random.seed(seed)
tf.random.set_seed(seed)



'''
auxiliary method converts input sequences to float vectors
'''
def convert_seq2vec(in_string):
	out_vec = []
	for i in range(0,len(in_string)):
		out_vec.append(nt_dict[in_string[i]])
	return keras.utils.to_categorical(out_vec,num_classes=max_features,dtype='float32')



'''
reads and prepares the data

transforms the output data with sklearn tools by minmax-scaling
'''
def prepare_data(input_file, prim_input_col, sec_input_col, metadata_cols, scaler_dir):
	#load raw data from csv
	if '.tsv' in input_file:
		sep='\t'
	else:
		sep=','

	raw_data = pd.read_csv(input_file, low_memory=False, sep=sep)
	raw_data.reset_index(inplace=True, drop=True)

	#only take short enough input data
	raw_data = raw_data.loc[(raw_data[prim_input_col].str.len() <= maxlen_prim)]
	raw_data[sec_input_col] = raw_data[sec_input_col].astype(str).str[:maxlen_sec]

	raw_test = raw_data

	#load scalers, scale metadata
	meta_test = pd.DataFrame()
	for md in metadata_cols:
		tsc = joblib.load(scaler_dir + md + '_scaler.gz')
		meta_test[md] = pd.DataFrame(tsc.transform(raw_test[md].values.reshape(-1,1)))

	output_scaler = joblib.load(scaler_dir+'output_scaler.gz')

	#one-hot encoding of sequence data
	tmp = raw_test[prim_input_col].to_numpy()
	tmp_test = []
	for t in tmp:
		tmp_test.append(convert_seq2vec(t))
	prim_seq_test = keras.utils.pad_sequences(tmp_test,maxlen=maxlen_prim,dtype='float32',padding='pre',value=-1.0)

	tmp = raw_test[sec_input_col].to_numpy()
	tmp_test = []
	for t in tmp:
		tmp_test.append(convert_seq2vec(t))
	sec_seq_test = keras.utils.pad_sequences(tmp_test,maxlen=maxlen_sec,dtype='float32',padding='post',value=-1.0)


	return prim_seq_test, sec_seq_test, meta_test, output_scaler, raw_test


'''
begin script
'''
max_features = 4  #4 nucleic bases
maxlen_prim = 7720  #maximum length of UTRs
maxlen_sec = 100  #first nts of CDS
prim_input_col="mutated_utr"
sec_input_col='CDS'
metadata_cols = ['UTR_length','log_ORF_length','number_exons','normalized_5p_folding_energy','GC_content','number_outframe_uAUGs','number_inframe_uAUGs']
pt_meta_cols = 5

scaler_dir = '../tl_TranslateLLM_HEK293/scalers/'
data_path = '../tl_TranslateLLM_HEK293/clinvar_mutated_sequences_with_metadata.tsv'
model_path = 'tl_TranslateLLM_HEK293.h5'


#nucleotide dictionary
nt_dict = {'A' : 0, 'C' : 1, 'G' : 2, 'T' : 3, 'a' : 0, 'c' : 1, 'g' : 2, 't' : 3}


#load and scale data
prim_seq_test, sec_seq_test, meta_test, output_scaler, raw_test = prepare_data(data_path, prim_input_col=prim_input_col, sec_input_col=sec_input_col, metadata_cols=metadata_cols, scaler_dir=scaler_dir)



#load model if code used in testing configuration
model = keras.models.load_model(model_path)

pred = output_scaler.inverse_transform(model.predict([[prim_seq_test,np.zeros(shape=(len(prim_seq_test),pt_meta_cols),dtype='float32')],sec_seq_test,meta_test]))[:,0]

raw_test['predicted_TE'] = pred
raw_test.to_csv("test_data_clinvar_TE.tsv",sep="\t",index=False)
